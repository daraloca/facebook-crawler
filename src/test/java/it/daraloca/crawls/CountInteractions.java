package it.daraloca.crawls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

import org.junit.Test;

import it.daraloca.FacebookCrawler;
import it.daraloca.components.PostRegistry;
import it.daraloca.pages.FacebookLoggedProfile;
import it.daraloca.pages.FacebookLoggedRegistry;
import it.daraloca.pages.FacebookPostSingle;

public class CountInteractions extends FacebookCrawler {

	private FacebookLoggedRegistry flr;

	private List<PostRegistry> fdgf;

	private List<String> linksOfDRLC = new ArrayList<String>();
	private List<String> linksOfHH = new ArrayList<String>();

	private List<String> counting = new ArrayList<String>();

	private HashMap<String, Double> map = new HashMap<String, Double>();

	@Test
	public void actions() {
		this.goToRegistryMyPost();
		this.makeVisibileAllPost();
		this.getLinksOfPost();
		this.iterateAllPost();

		this.listInHashMap();
		this.defineStats();

		this.goodPrint();

	}

	public class Print implements BiConsumer<String, Double> {
		@Override
		public void accept(String t, Double u) {
			System.out.println(t + " = " + u + "\n");
		}
	}

	private void goodPrint() {
		map.forEach(new Print());

	}

	public class Stats implements BiFunction<String, Double, Double> {
		@Override
		public Double apply(String t, Double u) {
			double i = u / (linksOfDRLC.size() + linksOfHH.size());
			return i;
		}
	}

	private void defineStats() {
		map.replaceAll(new Stats());
	}

	private void listInHashMap() {
		Double newVal = 0.0;
		for (String lab : counting) {
			if (map.containsKey(lab)) {
				newVal = map.get(lab) + 1.0;
				map.put(lab, newVal);
			} else {
				map.put(lab, 1.0);
			}
		}
	}

	private void goToRegistryMyPost() {
		FacebookLoggedProfile flp = super.loggedHome.getFacebookSideNav().goToProfile();
		flp.isPageLoaded();
		this.flr = flp.getFacebookProfileCover().clickToActivityRegistry();
		this.flr.isPageLoaded();
		this.flr.goToMyPost();
	}

	private void makeVisibileAllPost() {
		this.flr.scrollDownUntillVisibilityOf("month_2015_4");
		fdgf = this.flr.getAllPost();
	}

	private void getLinksOfPost() {
		for (PostRegistry rp : fdgf) {
			if (rp.getText().contains("HH")) {
				linksOfHH.add(rp.getLink());
			} else if (rp.getText().contains("DRLC")) {
				linksOfDRLC.add(rp.getLink());
			}
		}
	}

	private void iterateAllPost() {
		FacebookPostSingle fps;
		for (String link : linksOfDRLC) {
			this.getWebDriver().get(link);
			loggedHome.layerPresent();
			fps = new FacebookPostSingle();
			fps.clickForInteractionModal();
			counting.addAll(fps.getWhoLiked());
		}
		for (String link : linksOfHH) {
			this.getWebDriver().get(link);
			loggedHome.layerPresent();
			fps = new FacebookPostSingle();
			fps.clickForInteractionModal();
			counting.addAll(fps.getWhoLiked());
		}
	}

}
