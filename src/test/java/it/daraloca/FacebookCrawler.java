package it.daraloca;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import it.daraloca.pages.FacebookHomePage;
import it.daraloca.pages.FacebookLoggedHome;
import it.daraloca.utils.PageBeans;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = PageBeans.class)
public class FacebookCrawler {
	
	@Autowired
	private WebDriver wd;

	@Value("${project.login.email}")
	private String email;

	@Value("${project.login.password}")
	private String pass;

	protected FacebookLoggedHome loggedHome;

	@Before
	public void login() {
		FacebookHomePage fbh = new FacebookHomePage();
		this.loggedHome = fbh.login(email, pass);
	}

	@After
	public void quit() {
		wd.quit();
	}
	
	protected WebDriver getWebDriver(){
		return wd;
	}

}
