package it.daraloca.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import it.daraloca.components.FacebookProfileCover;

public class FacebookLoggedProfile extends FacebookLoggedPage {

	private FacebookProfileCover fpc;
	
	public FacebookLoggedProfile(){
		fpc = new FacebookProfileCover();
	}

	@Override
	protected ExpectedCondition<Boolean> subPageLoadCondition() {
		super.getFacebookPageBar().isPageLoaded();
		return ExpectedConditions
				.and(ExpectedConditions.presenceOfElementLocated(By.id("pagelet_timeline_main_column")));
	}
	
	public FacebookProfileCover getFacebookProfileCover(){
		return fpc;
	}

}
