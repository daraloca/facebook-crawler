package it.daraloca.pages;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.codeborne.selenide.Condition;

import it.daraloca.Loadable;
import it.daraloca.components.FacebookPageBar;

public abstract class FacebookLoggedPage extends Loadable {

	private FacebookPageBar bar;

	public FacebookLoggedPage() {
		bar = new FacebookPageBar();
	}

	@Override
	protected void isLoaded() throws Error {
		this.load();
	}

	@Override
	protected void load() {
		this.getWaiter().until(ExpectedConditions.and(ExpectedConditions.elementToBeClickable(By.id("pagelet_bluebar")),
				this.subPageLoadCondition()));
	}

	public FacebookPageBar getFacebookPageBar() {
		return bar;
	}
	
	public void layerPresent(){
		if($(".uiLayer").isDisplayed() || $(".uiLayer").is(Condition.visible)){
			$(".uiLayer").click();
		}
	}

	protected abstract ExpectedCondition<Boolean> subPageLoadCondition();

}
