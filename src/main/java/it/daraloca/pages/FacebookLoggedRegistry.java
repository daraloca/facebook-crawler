package it.daraloca.pages;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import it.daraloca.components.PostRegistry;

public class FacebookLoggedRegistry extends FacebookLoggedPage {

	public FacebookLoggedRegistry() {
	}

	@Override
	protected ExpectedCondition<Boolean> subPageLoadCondition() {
		super.getFacebookPageBar().isPageLoaded();
		return ExpectedConditions.and(ExpectedConditions.presenceOfElementLocated(By.id("sideNav")));
	}

	public FacebookLoggedRegistry goToMyPost() {
		$(By.id("navItem_cluster_11")).$(By.tagName("a")).click();
		return this;
	}

	/**
	 * 
	 * @param monthYear
	 *            example: "month_2015_4"
	 * @return
	 */
	public FacebookLoggedRegistry scrollDownUntillVisibilityOf(String id) {
		JavascriptExecutor js = ((JavascriptExecutor) super.wd);

		do {
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		} while (!$(By.id(id)).isDisplayed());

		return this;
	}

	public List<PostRegistry> getAllPost() {
		ElementsCollection elc = $$(By.cssSelector("div.uiBoxWhite table tbody"));
		List<PostRegistry> list = new ArrayList<PostRegistry>();
		for (SelenideElement se : elc) {
			if (se.isDisplayed()) {
				PostRegistry rp = new PostRegistry(se);
				list.add(rp);
			}
		}
		return list;
	}

}
