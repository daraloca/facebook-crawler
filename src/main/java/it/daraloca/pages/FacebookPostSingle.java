package it.daraloca.pages;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import it.daraloca.Loadable;

public class FacebookPostSingle extends Loadable {

	@Override
	protected void load() {
		this.getWaiter()
				.until(ExpectedConditions.and(ExpectedConditions.visibilityOfElementLocated(By.id("u_jsonp_5_2"))));

	}

	@Override
	protected void isLoaded() throws Error {
		this.load();

	}

	public void clickForInteractionModal() {
		if ($("div.UFIContainer a._2x4v").exists()) {
			$("div.UFIContainer a._2x4v").click();
			this.getWaiter()
					.until(ExpectedConditions.or(
							ExpectedConditions.visibilityOfElementLocated(By.cssSelector("ul[id*='reaction_profile_browser']"))));

		}
	}

	public List<String> getWhoLiked() {
		List<String> list = new ArrayList<String>();
		ElementsCollection elList = $$("div.fsl.fwb.fcb a");
		for (SelenideElement se : elList) {
			list.add(se.getText());
		}
		return list;
	}

}
