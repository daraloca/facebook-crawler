package it.daraloca.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import it.daraloca.components.FacebookSideNav;

public class FacebookLoggedHome extends FacebookLoggedPage {

	private FacebookSideNav fsn;

	public FacebookLoggedHome() {
		fsn = new FacebookSideNav();
	}

	@Override
	protected ExpectedCondition<Boolean> subPageLoadCondition() {
		super.getFacebookPageBar().isPageLoaded();
		fsn.isPageLoaded();
		return ExpectedConditions.and(ExpectedConditions.presenceOfElementLocated(By.tagName("body")));
	}

	public FacebookSideNav getFacebookSideNav() {
		this.layerPresent();
		return fsn;
	}

}
