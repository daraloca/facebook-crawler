package it.daraloca.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import it.daraloca.Loadable;

public class FacebookHomePage extends Loadable {

	public FacebookHomePage(){
		wd.get("https://www.facebook.com");
	}

	@Override
	protected void isLoaded() throws Error {
		this.load();
	}

	@Override
	protected void load() {
		this.getWaiter().until(ExpectedConditions.and(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("email")),
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.name("pass")),
				ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("loginbutton"))));

	}

	public FacebookLoggedHome login(String email, String pass) {
		wd.findElement(By.name("email")).sendKeys(email);
		wd.findElement(By.name("pass")).sendKeys(pass);
		wd.findElement(By.id("loginbutton")).click();
		return new FacebookLoggedHome();
	}

}
