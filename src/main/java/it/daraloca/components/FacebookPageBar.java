package it.daraloca.components;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import it.daraloca.Loadable;

public class FacebookPageBar extends Loadable {

	@Override
	protected void isLoaded() throws Error {
		this.load();
	}

	@Override
	protected void load() {
		this.getWaiter()
				.until(ExpectedConditions.and(ExpectedConditions.visibilityOfElementLocated(By.id("pagelet_bluebar"))));

	}

}
