package it.daraloca.components;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import it.daraloca.Loadable;
import it.daraloca.pages.FacebookLoggedRegistry;

public class FacebookProfileCover extends Loadable {

	@Override
	protected void isLoaded() throws Error {
		this.load();
	}

	@Override
	protected void load() {
		this.getWaiter().until(
				ExpectedConditions.and(ExpectedConditions.visibilityOfElementLocated(By.id("timeline_top_section"))));

	}

	public FacebookLoggedRegistry clickToActivityRegistry() {
		$(By.id("activityLogButton")).click();
		return new FacebookLoggedRegistry();
	}

}
