package it.daraloca.components;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import com.codeborne.selenide.SelenideElement;

import it.daraloca.Loadable;

public class PostRegistry extends Loadable {

	private final SelenideElement se;

	public PostRegistry(SelenideElement el) {
		se = el;
	}

	@Override
	protected void isLoaded() throws Error {
		this.load();
	}

	@Override
	protected void load() {
	}

	public void goToPostDetail() {
		Actions action = new Actions(super.wd);
		action.moveToElement(se.findElement(By.className("fss")).findElement(By.tagName("a")));
		action.click();
		action.build().perform();
	}

	public String getLink() {
		return se.findElement(By.className("fss")).findElement(By.tagName("a")).getAttribute("href");
	}

	public String getText() {
		if (se.$(By.className("fsm")).$(By.tagName("div")).exists()) {
			String visible = se.$(By.className("fsm")).$(By.tagName("div")).getText();
			String invisible = se.$(By.className("fsm")).$(By.cssSelector("span.text_exposed_show")).getText();
			return visible + invisible;
		} else {
			if (se.$(By.className("fsm")).exists()) {
				return se.$(By.className("fsm")).getText();
			}
		}
		return "";
	}

}
