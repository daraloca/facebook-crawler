package it.daraloca.components;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import it.daraloca.Loadable;
import it.daraloca.pages.FacebookLoggedProfile;

public class FacebookSideNav extends Loadable {

	@Override
	protected void isLoaded() throws Error {
		this.load();
	}

	@Override
	protected void load() {
		this.getWaiter().until(ExpectedConditions.and(ExpectedConditions.visibilityOfElementLocated(By.id("leftCol")),
				ExpectedConditions.elementToBeClickable(By.id("userNav"))));

	}

	public FacebookLoggedProfile goToProfile() {
		$(By.id("userNav")).click();
		return new FacebookLoggedProfile();
	}

}
