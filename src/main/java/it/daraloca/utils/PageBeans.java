package it.daraloca.utils;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.codeborne.selenide.WebDriverRunner;

@Configuration
@PropertySource("classpath:/application.properties")
@ComponentScan(basePackages = { "it.daraloca", "it.daraloca.components", "it.daraloca.pages" })
public class PageBeans {

	@Value("${project.webdriver.chrome.path}")
	private String chromeDriverPath;

	private WebDriver wd;

	@Bean
	WebDriver getWebDriver() {
		File file = new File(chromeDriverPath);
		System.setProperty("webdriver.firefox.bin", file.getAbsolutePath());
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());

		if (!WebDriverRunner.hasWebDriverStarted()) {
			wd = new ChromeDriver();
			WebDriverRunner.setWebDriver(wd);
			wd.manage().window().maximize();
		}

		return WebDriverRunner.getAndCheckWebDriver();
	}
	
	
	
	

}
