package it.daraloca;

import org.springframework.boot.SpringApplication;

public class FacebookCrawlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FacebookCrawlerApplication.class, args);
	}
}
