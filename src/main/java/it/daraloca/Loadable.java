package it.daraloca;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import it.daraloca.utils.PageBeans;

public abstract class Loadable extends LoadableComponent<Loadable> {

	@Autowired
	protected WebDriver wd;

	public Loadable() {
		@SuppressWarnings("resource")
		AnnotationConfigApplicationContext appContext = new AnnotationConfigApplicationContext();
		appContext.register(PageBeans.class);
		appContext.refresh();
		wd = appContext.getBean(WebDriver.class);
	}

	public WebDriverWait getWaiter() {
		return new WebDriverWait(wd, 60, 1500);
	}

	public void isPageLoaded() {
		this.load();
		this.isLoaded();
	}

}
